raise Exception("This is just a template, fill it before use")

DATABASE_ENGINE = 'django.db.backends.sqlite3'
DATABASE_NAME = "db/default.db"
DATABASE_USER = ''
DATABASE_PASSWORD = ''
DATABASE_PORT = ''
DATABASE_HOST = ''

DEBUG = True
SECRET_KEY = "secret key string"
